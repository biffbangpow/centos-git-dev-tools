Just some CentOS images with some dev tools for when we need to quickly try something locally on the same OS as our web hosts.

Supported tags and respective Dockerfile links

* 7.2 (7.6/Dockerfile-7.2)
* 6.6 (7.6/Dockerfile-6.6)
* 6.5 (7.6/Dockerfile-6.5)